package com.androidtutorialshub.loginregister.model;

/**
 * Created by lalit on 9/12/2016.
 */
public class User {

    private int id;
    private String name;
    private String email;
    private String password;
    private String disease;
    private String dateofmedication;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String password) {
        this.disease = disease;
    }

    public String getDateofmedication() {
        return dateofmedication;
    }

    public void setDateofmedication(String password) {
        this.dateofmedication = dateofmedication;
    }

//    public int getUType() {
//        return utype;
//    }
//
//    public void setUType(int id) {
//        this.id = utype;
//    }
}
